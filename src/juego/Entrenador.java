/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego;
import java.util.ArrayList;
/**
 *
 * @author joker
 */
public class Entrenador {
private String nombreJugador;
    private String gamertag;
    private int Edad;
    private int cantidadVictorias;
    private int cantidadDerrotas;
    
    public Entrenador(String nombreJugador,String gamertag,int Edad){
        this.nombreJugador=nombreJugador;
        this.gamertag=gamertag;
        this.Edad=Edad;        
    }


    public String getNombreJugador() {
        return nombreJugador;
    }

    public void setNombreJugador(String nombreJugador) {
        this.nombreJugador = nombreJugador;
    }

    public String getGamertag() {
        return gamertag;
    }

    public void setGamertag(String gamertag) {
        this.gamertag = gamertag;
    }

    public int getEdad() {
        return Edad;
    }

    public void setEdad(int Edad) {
        this.Edad = Edad;
    }

    public int getCantidadVictorias() {
        return cantidadVictorias;
    }

    public void setCantidadVictorias(int cantidadVictorias) {
        this.cantidadVictorias = cantidadVictorias;
    }

    public int getCantidadDerrotas() {
        return cantidadDerrotas;
    }

    public void setCantidadDerrotas(int cantidadDerrotas) {
        this.cantidadDerrotas = cantidadDerrotas;
    }
}
